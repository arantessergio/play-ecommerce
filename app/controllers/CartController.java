package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.ExpressionList;
import models.Cart;
import models.Item;
import models.User;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.*;

public class CartController extends Controller {

    @Inject
    private MailerService mailer;

    @Transactional
    public Result create() {

        JsonNode json = request().body().asJson();

        Cart cart = Json.fromJson(json, Cart.class);

        cart.save();

        return ok();

    }

    @Transactional
    public Result remove(UUID id) {

        Cart cart = Cart.find.byId(id);

        if (cart == null) {

            return notFound();

        } else {

            boolean hasRemoved = cart.delete();

            if (hasRemoved) {

                return ok(getSuccessObject());

            } else {

                return badRequest();

            }

        }

    }


    @Transactional
    public Result view(UUID id) {

        User user = User.find.byId(id);

        if (user == null) {

            return notFound();

        } else {

            ExpressionList<Cart> query = Cart.find.query()
                    .where().eq("user_id", user.getId())
                    .where().eq("open", true);

            Cart cart = query.findOne();

            if (cart == null) {

                return ok(getEmptyArrayObject());

            } else {

                return ok(Json.toJson(cart));

            }


        }

    }

    @Transactional
    public Result listItemsByCart(UUID id) {

        Cart cart = Cart.find.byId(id);

        if (cart == null) {

            return notFound();

        } else {

            ExpressionList<Item> query = Item.find.query().where()
                    .eq("cart_id", cart.getId());

            List<Item> result = query.findList();

            return ok(Json.toJson(result));

        }

    }

    @Transactional
    public Result addItem(UUID id) {

        try {

            User user = User.find.byId(id);

            if (user == null) {

                return notFound();

            } else {

                ExpressionList<Cart> query = Cart.find.query()
                        .where().eq("user_id", user.getId())
                        .where().eq("open", true);

                Cart cart = query.findOne();

                JsonNode json = request().body().asJson();

                Item item = Json.fromJson(json, Item.class);


                if (cart == null) {

                    cart = new Cart();

                    cart.setUser(user);

                    cart.setOpen(Boolean.TRUE);

                    cart.save();

                    item.setCart(cart);

                    item.save();

                    return ok(getSuccessObject());

                } else {

                    item.setCart(cart);

                    item.save();

                    return ok(getSuccessObject());

                }

            }

        } catch (Exception e) {

            return internalServerError(getErrorObject());

        }

    }

    @Transactional
    public Result updateItem(UUID id) {

        Item dbItem = Item.find.byId(id);

        if (dbItem == null) {

            return notFound();

        } else {

            JsonNode json = request().body().asJson();

            Item requestItem = Json.fromJson(json, Item.class);

            dbItem.setAmount(requestItem.getAmount());
            dbItem.setTotal(requestItem.getTotal());

            dbItem.save();

            return ok(getSuccessObject());

        }

    }

    @Transactional
    public Result removeItem(UUID id) {

        try {
            Item dbItem = Item.find.byId(id);

            if (dbItem == null) {

                return notFound();

            } else {

                boolean hasDeleted = dbItem.delete();

                if (hasDeleted) {

                    return ok(getSuccessObject());

                } else {

                    return badRequest(getErrorObject());

                }

            }

        } catch (Exception e) {

            return internalServerError(getErrorObject());

        }


    }

    @Transactional
    public Result finishOrder(UUID id) {

        try {

            User user = User.find.byId(id);

            if (user == null) {

                return notFound();

            } else {

                ExpressionList<Cart> query = Cart.find.query()
                        .where().eq("user_id", user.getId())
                        .where().eq("open", 1);


                Cart cart = query.findOne();

                if (cart == null) {

                    return ok(getSuccessObject());

                } else {

                    cart.setOpen(Boolean.FALSE);

                    cart.save();

                    mailer.sendEmail(user.getEmail());

                    return ok(getSuccessObject());

                }


            }

        } catch (Exception e) {

            return internalServerError(getErrorObject());

        }


    }

    private JsonNode getEmptyArrayObject() {

        Map<String, List> map = new HashMap();

        map.put("items", new ArrayList<Item>());

        return Json.toJson(map);

    }

    private JsonNode getErrorObject() {

        Map<String, Boolean> map = new HashMap();

        map.put("success", Boolean.FALSE);

        return Json.toJson(map);
    }

    private JsonNode getSuccessObject() {

        Map<String, Boolean> map = new HashMap();

        map.put("success", Boolean.TRUE);

        return Json.toJson(map);
    }

}
