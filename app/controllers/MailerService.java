package controllers;

import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;

import javax.inject.Inject;

public class MailerService {

    @Inject
    MailerClient mailerClient;

    public void sendEmail(final String to) {
        Email email = new Email()
                .setSubject("no-reply")
                .setFrom("playappssergio@gmail.com")
                .addTo(to)
                .setBodyText("Obrigado pela sua compra com Lojinha.");
        mailerClient.send(email);
    }
}