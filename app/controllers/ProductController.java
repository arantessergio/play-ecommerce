package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Product;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import java.util.UUID;

public class ProductController extends Controller {

    public Result list() {

        List<Product> list = Product.find.all();

        JsonNode json = Json.toJson(list);

//        return HeadersApplyer.okWithHeaders(json, this);

        return ok(json);

    }

    @Transactional
    public Result create() {

        JsonNode json = request().body().asJson();

        Product product = Json.fromJson(json, Product.class);

        product.save();

//        return HeadersApplyer.okWithHeaders(null, this);

        return created();

    }

    public Result update(UUID id) {

        Product product = Product.find.byId(id);

        if (product == null) {

//            return HeadersApplyer.notFoundWithHeaders(this);

            return notFound();

        } else {

            JsonNode requestJson = request().body().asJson();

            Product requestProduct = Json.fromJson(requestJson, Product.class);

            product.setName(requestProduct.getName());
            product.setPrice(requestProduct.getPrice());

            product.save();

//            return HeadersApplyer.okWithHeaders(Json.toJson(product), this);

            return created(Json.toJson(product));

        }

    }

    public Result get(UUID id) {

        Product product = Product.find.byId(id);

        if (product == null) {

            return notFound();

        } else {

            return ok(Json.toJson(product));

        }

    }

    public Result remove(UUID id) {

        Product product = Product.find.byId(id);

        if (product == null) {

//            return HeadersApplyer.notFoundWithHeaders(this);
            return notFound();

        } else {

            boolean hasDeleted = product.delete();

            if (hasDeleted) {

//                return HeadersApplyer.okWithHeaders(null, this);

                return ok();

            } else {

//                return HeadersApplyer.badRequestWithHeaders(this);

                return badRequest();

            }

        }

    }

}
