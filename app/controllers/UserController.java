package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.ExpressionList;
import models.User;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.HashMap;
import java.util.Map;

public class UserController extends Controller {


    public Result create() {

        try {

            JsonNode json = request().body().asJson();

            User user =  Json.fromJson(json, User.class);


            ExpressionList<User> query = User.find.query().where().eq("email", user.getEmail());

            User dbUser = query.findOne();

            if (dbUser != null) {

                return ok(hasUserObject());

            } else {

                user.save();

                return ok(Json.toJson(user));

            }


        } catch (Exception e) {

            return internalServerError();

        }


    }

    public Result auth() {

        JsonNode json = request().body().asJson();

        User user = Json.fromJson(json, User.class);

        ExpressionList<User> query = User.find.query().where()
                .eq("email", user.getEmail())
                .eq("password", user.getPassword());

        User dbUser = query.findOne();

        if (dbUser == null) {

            return notFound();

        } else {

            return ok(Json.toJson(dbUser));

        }

    }

    private JsonNode hasUserObject() {

        Map<String, Boolean> map = new HashMap<>();

        map.put("success", Boolean.FALSE);

        return Json.toJson(map);

    }


}