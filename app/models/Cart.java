package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class Cart extends Model {

    @Id
    private UUID id;

    @ManyToOne
    @JsonBackReference
    private User user;

    @OneToMany(mappedBy = "cart")
    private List<Item> items;

    private Boolean open;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public static Finder<UUID, Cart> find = new Finder<>(Cart.class);

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
