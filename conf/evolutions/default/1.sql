# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table cart (
  id                            varchar(40) not null,
  user_id                       varchar(40),
  open                          tinyint(1) default 0,
  constraint pk_cart primary key (id)
);

create table item (
  id                            varchar(40) not null,
  product_id                    varchar(40),
  amount                        decimal(38),
  total                         decimal(38),
  cart_id                       varchar(40),
  constraint pk_item primary key (id)
);

create table product (
  id                            varchar(40) not null,
  name                          varchar(255),
  price                         decimal(38),
  constraint pk_product primary key (id)
);

create table user (
  id                            varchar(40) not null,
  email                         varchar(255),
  password                      varchar(255),
  constraint pk_user primary key (id)
);

alter table cart add constraint fk_cart_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_cart_user_id on cart (user_id);

alter table item add constraint fk_item_product_id foreign key (product_id) references product (id) on delete restrict on update restrict;
create index ix_item_product_id on item (product_id);

alter table item add constraint fk_item_cart_id foreign key (cart_id) references cart (id) on delete restrict on update restrict;
create index ix_item_cart_id on item (cart_id);


# --- !Downs

alter table cart drop foreign key fk_cart_user_id;
drop index ix_cart_user_id on cart;

alter table item drop foreign key fk_item_product_id;
drop index ix_item_product_id on item;

alter table item drop foreign key fk_item_cart_id;
drop index ix_item_cart_id on item;

drop table if exists cart;

drop table if exists item;

drop table if exists product;

drop table if exists user;

