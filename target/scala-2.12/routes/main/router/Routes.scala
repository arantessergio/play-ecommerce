// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/sergio/shopping/conf/routes
// @DATE:Sun Apr 22 15:48:35 BRT 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_0: controllers.HomeController,
  // @LINE:9
  Assets_4: controllers.Assets,
  // @LINE:11
  UserController_2: controllers.UserController,
  // @LINE:14
  ProductController_1: controllers.ProductController,
  // @LINE:20
  CartController_3: controllers.CartController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_0: controllers.HomeController,
    // @LINE:9
    Assets_4: controllers.Assets,
    // @LINE:11
    UserController_2: controllers.UserController,
    // @LINE:14
    ProductController_1: controllers.ProductController,
    // @LINE:20
    CartController_3: controllers.CartController
  ) = this(errorHandler, HomeController_0, Assets_4, UserController_2, ProductController_1, CartController_3, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, Assets_4, UserController_2, ProductController_1, CartController_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users""", """controllers.UserController.create()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/auth""", """controllers.UserController.auth()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """controllers.ProductController.list()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products""", """controllers.ProductController.create()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.update(id:java.util.UUID)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.remove(id:java.util.UUID)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.get(id:java.util.UUID)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """carts""", """controllers.CartController.create()"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """carts/""" + "$" + """id<[^/]+>""", """controllers.CartController.remove(id:java.util.UUID)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>/cart""", """controllers.CartController.view(id:java.util.UUID)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>/cart/finish""", """controllers.CartController.finishOrder(id:java.util.UUID)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """users/""" + "$" + """id<[^/]+>/cart""", """controllers.CartController.addItem(id:java.util.UUID)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """items/""" + "$" + """id<[^/]+>""", """controllers.CartController.updateItem(id:java.util.UUID)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """items/""" + "$" + """id<[^/]+>""", """controllers.CartController.removeItem(id:java.util.UUID)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_4.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_UserController_create2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users")))
  )
  private[this] lazy val controllers_UserController_create2_invoker = createInvoker(
    UserController_2.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "create",
      Nil,
      "POST",
      this.prefix + """users""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_UserController_auth3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/auth")))
  )
  private[this] lazy val controllers_UserController_auth3_invoker = createInvoker(
    UserController_2.auth(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.UserController",
      "auth",
      Nil,
      "POST",
      this.prefix + """users/auth""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_ProductController_list4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_ProductController_list4_invoker = createInvoker(
    ProductController_1.list(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "list",
      Nil,
      "GET",
      this.prefix + """products""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_ProductController_create5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products")))
  )
  private[this] lazy val controllers_ProductController_create5_invoker = createInvoker(
    ProductController_1.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "create",
      Nil,
      "POST",
      this.prefix + """products""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_ProductController_update6_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_update6_invoker = createInvoker(
    ProductController_1.update(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "update",
      Seq(classOf[java.util.UUID]),
      "PUT",
      this.prefix + """products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_ProductController_remove7_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_remove7_invoker = createInvoker(
    ProductController_1.remove(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "remove",
      Seq(classOf[java.util.UUID]),
      "DELETE",
      this.prefix + """products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_ProductController_get8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_get8_invoker = createInvoker(
    ProductController_1.get(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "get",
      Seq(classOf[java.util.UUID]),
      "GET",
      this.prefix + """products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_CartController_create9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("carts")))
  )
  private[this] lazy val controllers_CartController_create9_invoker = createInvoker(
    CartController_3.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "create",
      Nil,
      "POST",
      this.prefix + """carts""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_CartController_remove10_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("carts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CartController_remove10_invoker = createInvoker(
    CartController_3.remove(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "remove",
      Seq(classOf[java.util.UUID]),
      "DELETE",
      this.prefix + """carts/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_CartController_view11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true), StaticPart("/cart")))
  )
  private[this] lazy val controllers_CartController_view11_invoker = createInvoker(
    CartController_3.view(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "view",
      Seq(classOf[java.util.UUID]),
      "GET",
      this.prefix + """users/""" + "$" + """id<[^/]+>/cart""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_CartController_finishOrder12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true), StaticPart("/cart/finish")))
  )
  private[this] lazy val controllers_CartController_finishOrder12_invoker = createInvoker(
    CartController_3.finishOrder(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "finishOrder",
      Seq(classOf[java.util.UUID]),
      "POST",
      this.prefix + """users/""" + "$" + """id<[^/]+>/cart/finish""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_CartController_addItem13_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("users/"), DynamicPart("id", """[^/]+""",true), StaticPart("/cart")))
  )
  private[this] lazy val controllers_CartController_addItem13_invoker = createInvoker(
    CartController_3.addItem(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "addItem",
      Seq(classOf[java.util.UUID]),
      "POST",
      this.prefix + """users/""" + "$" + """id<[^/]+>/cart""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_CartController_updateItem14_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("items/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CartController_updateItem14_invoker = createInvoker(
    CartController_3.updateItem(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "updateItem",
      Seq(classOf[java.util.UUID]),
      "PUT",
      this.prefix + """items/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_CartController_removeItem15_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("items/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CartController_removeItem15_invoker = createInvoker(
    CartController_3.removeItem(fakeValue[java.util.UUID]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "removeItem",
      Seq(classOf[java.util.UUID]),
      "DELETE",
      this.prefix + """items/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index)
      }
  
    // @LINE:9
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_4.versioned(path, file))
      }
  
    // @LINE:11
    case controllers_UserController_create2_route(params@_) =>
      call { 
        controllers_UserController_create2_invoker.call(UserController_2.create())
      }
  
    // @LINE:12
    case controllers_UserController_auth3_route(params@_) =>
      call { 
        controllers_UserController_auth3_invoker.call(UserController_2.auth())
      }
  
    // @LINE:14
    case controllers_ProductController_list4_route(params@_) =>
      call { 
        controllers_ProductController_list4_invoker.call(ProductController_1.list())
      }
  
    // @LINE:15
    case controllers_ProductController_create5_route(params@_) =>
      call { 
        controllers_ProductController_create5_invoker.call(ProductController_1.create())
      }
  
    // @LINE:16
    case controllers_ProductController_update6_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_ProductController_update6_invoker.call(ProductController_1.update(id))
      }
  
    // @LINE:17
    case controllers_ProductController_remove7_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_ProductController_remove7_invoker.call(ProductController_1.remove(id))
      }
  
    // @LINE:18
    case controllers_ProductController_get8_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_ProductController_get8_invoker.call(ProductController_1.get(id))
      }
  
    // @LINE:20
    case controllers_CartController_create9_route(params@_) =>
      call { 
        controllers_CartController_create9_invoker.call(CartController_3.create())
      }
  
    // @LINE:21
    case controllers_CartController_remove10_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_remove10_invoker.call(CartController_3.remove(id))
      }
  
    // @LINE:22
    case controllers_CartController_view11_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_view11_invoker.call(CartController_3.view(id))
      }
  
    // @LINE:24
    case controllers_CartController_finishOrder12_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_finishOrder12_invoker.call(CartController_3.finishOrder(id))
      }
  
    // @LINE:25
    case controllers_CartController_addItem13_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_addItem13_invoker.call(CartController_3.addItem(id))
      }
  
    // @LINE:26
    case controllers_CartController_updateItem14_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_updateItem14_invoker.call(CartController_3.updateItem(id))
      }
  
    // @LINE:27
    case controllers_CartController_removeItem15_route(params@_) =>
      call(params.fromPath[java.util.UUID]("id", None)) { (id) =>
        controllers_CartController_removeItem15_invoker.call(CartController_3.removeItem(id))
      }
  }
}
